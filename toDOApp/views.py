from django.shortcuts import render
from models import todo
from django.http import HttpResponse
from django.template import RequestContext, loader


# Create your views here.
def index(request):
	#return HttpResponse("Hello, world. You're at the polls index.")
	items = todo.objects.all()
	template = loader.get_template('toDOApp/index.html')
	context = RequestContext(request, {'items': items,})
	return HttpResponse(template.render(context))
	